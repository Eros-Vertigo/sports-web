import Vue from 'vue'
import App from './App'
import uView from "uview-ui";
import common from "common/common.js";
Vue.use(uView);

Vue.config.productionTip = false

App.mpType = 'app'

Vue.prototype.$common = common;

const app = new Vue({
    ...App
})
app.$mount()
