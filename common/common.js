import config from "./config.js";

const baseUrl = config.devUrl;

/**
 * 封装请求方法
 * @param  {[type]} url    [description]
 * @param  {String} method [description]
 * @param  {Object} data   [description]
 * @return {[type]}        [description]
 */
const request = (url, method = "", data = {}, check_login = false) => {

	let header = {
		"Content-Type": "application/json",
	}
	let token = uni.getStorageSync('token');
	if (check_login && !token) {
		showtoast('请先登录后操作');
		setTimeout(function(){
			let routes = getCurrentPages(); // 获取当前打开过的页面路由数组

			toroute('/modelA/page/login', routes.length == 1 ? 1 : 2);
		}, 500);
		return;
	}
	if (token) {
		data.token = uni.getStorageSync('token');
	}
	return new Promise((resolve, reject) => {
		method = method ? method : "POST"
		uni.request({
			url   : baseUrl+url,
			method: method,
			header: header,
			data  : data,
			success: (res) => {
				// console.log('success')
				resolve(res.data)
			},
			fail: (err) => {
				console.log(err)
				showtoast('请求失败');
				reject(err)
			},
			complete: () => {

			}
		})
	})
}

/**
 * 上传图片
 * @param  {[type]} url      [description]
 * @param  {[type]} filePath [description]
 * @return {[type]}          [description]
 */
const uploadfile = (url, tempFilePaths) => {
	console.log(tempFilePaths)
	return new Promise((resolve, reject)=>{
		uni.uploadFile({
			url     : baseUrl + url, //仅为示例，非真实的接口地址
			filePath: tempFilePaths[0],
			name    : 'iFile',
			formData: {
	            token : uni.getStorageSync("token")
	        },
	        success: (uploadFileRes) => {
	        	uploadFileRes = JSON.parse(uploadFileRes.data);
	            if (uploadFileRes.code == 1) {
	            	showtoast(uploadFileRes.msg);
	            	resolve(uploadFileRes.data);
	            } else {
	            	showtoast(uploadFileRes.msg);
	            }
	        },
	        fail: (err) => {
	        	reject(err);
	        }
	    });
	});
}
/**
 * 跳转方法
 * @param  {[type]} url         [description]
 * @param  {Number} type        [description]
 * @param  {Number} check_login [description]
 * @return {[type]}             [description]
 */
const toroute = (url, type = 1, check_login = "") => {
	if (check_login && !uni.getStorageSync('token')) {
		showtoast('请先登录后操作');
		setTimeout(function(){
			toroute('/modelA/page/login');
		}, 500);
		return;
	}
	switch (type) {
		case 1:
			// navigateTo
			uni.navigateTo({
				url: url
			});
			break;
		case 2:
			// redirectTo
			uni.redirectTo({
				url: url
			});
			break;
		case 3:
			// reLaunch
			uni.reLaunch({
				url: url
			});
			break;
		case 4:
			// switchTab
			uni.switchTab({
				url: url
			});
			break;
		case 5:
			// navigateBack
			uni.navigateBack();
			break;
		default:
			// navigateTo
			uni.navigateTo({
				url: url
			});
			break;
	}
}
/**
 * 提示框
 * @param  {[type]} title [description]
 * @return {[type]}       [description]
 */
const showtoast = (title, check_back = false) => {
	uni.showToast({
		title : title,
		icon : 'none',
		complete () {
			if (check_back) {
				setTimeout(function (){
					toroute('', 5);
				}, 500);
			}
		}
	})
}

/**
 * 提示模态框
 * @param  {String} title   [description]
 * @param  {[type]} content [description]
 * @return {[type]}         [description]
 */
const showmodal = (title = "提示", content) => {
	uni.showModal({
		title 	: title,
		content : content,
		success : function(res) {
			if (res.confirm) {
				// console.log("用户点击确定");
			} else if (res.cancel) {
				// console.log("用户点击取消");
			}
		},
	});
}

// 输入校验
const pregcheck = (str, regexp = "", compare = "") => {
	if (!str || str == "") {
		return false;
	}
	if (regexp && !regexpCheck(regexp).test(str)) {
		return false;
	}
	if (compare && str != compare) {
		return false;
	}
	return true;
}

/**
 * 正则
 * @param  {[type]} type [description]
 * @return {[type]}      [description]
 */
function regexpCheck(type){
	switch (type) {
		case "phone":
			return /^0?1[3|4|5|6|7|8][0-9]\d{8}$/;
		break;
	}
}

/**
 * 选择图片
 * @param  {[type]} count [description]
 * @return {[type]}       [description]
 */
const chooseImage = (count) => {
	return new Promise((resolve, reject)=> {
		uni.chooseImage({
			count 		: count, //上传照片数量
			sizeType	: ["original", "compressed"], //可以指定是原图还是压缩图，默认二者都有
			sourceType	: ["album", "camera"], //从相册选择
			success 	: function(chooseImageRes) {
				if (chooseImageRes.tempFiles[0].size >= 524288000) {
					showmodal("提示", "文件超过500M，请重新选择");
					return;
				}
				const tempFilePaths = chooseImageRes.tempFilePaths;
		        
				resolve(uploadfile("?s=/api/upload/image", tempFilePaths));
			},
			fail: (err) => { reject(err); }
		});
	});
}

/**
 * 获取页面元素大小和文职
 * @param  {[type]} selector [description]
 * @return {[type]}          [description]
 */
const getRect = (selector) => {
	return new Promise((resolve) => {
		let view = uni.createSelectorQuery().select(selector);
		view.fields({
			size: true,
			rect: true,
			scrollOffset:true
		}, (res) => {
			resolve(res);
		}).exec();
	})
}
export default {
	request    : request,
	toroute    : toroute,
	showtoast  : showtoast,
	pregcheck  : pregcheck,
	chooseImage: chooseImage,
	getRect    : getRect
}
